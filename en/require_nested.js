//root = English
define({
  menuItemApplications: 'Applications',
  menuItemAudio: 'Audio',
  menuItemImage: 'Image',
  menuItemOverlay: 'Overlay',
  menuItemPrivacyMask: 'Privacy Mask',
  menuItemPtz: 'PTZ',
  menuItemStream: 'Stream',
  menuItemViewArea: 'View Area'
});
