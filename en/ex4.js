{
    "glossary": {
        "1": "example glossary",
		"none": {
            "2": "S",
			"GlossList": {
                "GlossEntry": {
                    "3": "SGML1",
					"4": "SGML2",
					"5": "Standard Generalized Markup Language",
					"6": "SGML3",
					"7": "ISO 8879:1986",
					"GlossDef": {
                        "8": "A meta-markup language, used to create markup languages such as DocBook.",
						"11": ["GML", "XML"]
                    },
					"10": "markup"
                }
            }
        }
    }
}
