{"menu": {
    "id1": "1",
    "none": [
        {"id2": "test"},
        {"id3": "OpenNew", "id4": "Open New"},
        null,
        {"id5": "ZoomIn", "1d6": "Zoom In"},
        {"id7": "ZoomOut", "id8": "Zoom Out"},
        {"id9": "OriginalView", "id10": "Original View"},
        null,
        {"id11": "Quality"},
        {"id12": "Pause"},
        {"id13": "Mute"},
        null,
        {"id14": "Find", "id15": "Find..."},
        {"id16": "FindAgain", "id17": "Find Again"},
        {"id18": "Copy"},
        {"id19": "CopyAgain", "id20": "Copy Again"},
        {"id21": "CopySVG", "id22": "Copy SVG"},
        {"id23": "ViewSVG", "id24": "View SVG"},
        {"id25": "ViewSource", "id26": "View Source"},
        {"id27": "SaveAs", "id28": "Save As"},
        null,
        {"id29": "Help"},
        {"id30": "About", "31": "About Adobe CVG Viewer..."}
    ]
}}
