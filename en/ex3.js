{"web-app": {
  "servlet": [   
    {
      "1": "cofaxCDS",
      "2": "org.cofax.cds.CDSServlet",
      "init-param": {
        "2": "Philadelphia, PA",
        "3": "ksm@pobox.com",
        "4": "Cofax",
        "5": "/images/cofax.gif",
        "6": "/content/static",
        "7": "org.cofax.WysiwygTemplate",
        "8": "org.cofax.FilesTemplateLoader",
        "9": "templates",
        "templateOverridePath": "",
        "10": "listTemplate.htm",
        "11": "articleTemplate.htm",
        "useJSP": false,
        "12": "listTemplate.jsp",
        "13": "articleTemplate.jsp",
        "cachePackageTagsTrack": 200,
        "cachePackageTagsStore": 200,
        "cachePackageTagsRefresh": 60,
        "cacheTemplatesTrack": 100,
        "cacheTemplatesStore": 50,
        "cacheTemplatesRefresh": 15,
        "cachePagesTrack": 200,
        "cachePagesStore": 100,
        // Comment
        "cachePagesRefresh": 10,
        "cachePagesDirtyRead": 10,
        "14": "forSearchEnginesList.htm",
        /* Comment 
         *
         * "test": "test",
         */
        "15": "forSearchEngines.htm",
        "16": "WEB-INF/robots.db",
        "useDataStore": true, // comment again
        "17": "org.cofax.SqlDataStore",
        "18": "org.cofax.SqlRedirection",
        /*
         *
         * comment 2
         *
         */
        "19": "cofax",
        "20": "com.microsoft.jdbc.sqlserver.SQLServerDriver",
        "21": "jdbc:microsoft:sqlserver://LOCALHOST:1433;DatabaseName=goon", // comment
        "22": "sa",
        "23": "dataStoreTestQuery",
        "24": "SET NOCOUNT ON;select test='test';",
        "25": "/usr/local/tomcat/logs/datastore.log",
        "dataStoreInitConns": 10,
        "dataStoreMaxConns": 100,
        "dataStoreConnUsageLimit": 100,
        "26": "debug",
        "maxUrlLength": 500}},
    {
      "27": "cofaxEmail",
      "28": "org.cofax.cds.EmailServlet",
      "init-param": {
      "29": "mail1",
      "30": "mail2"}},
    {
      "31": "cofaxAdmin",
      "32": "org.cofax.cds.AdminServlet"},
 
    {
      "33": "fileServlet",
      "34": "org.cofax.cds.FileServlet"},
    {
      "35": "cofaxTools",
      "36": "org.cofax.cms.CofaxToolsServlet",
      "init-param": {
        "37": "toolstemplates/",
        "log": 1,
        "38": "/usr/local/tomcat/logs/CofaxTools.log",
        "logMaxSize": "",
        "dataLog": 1,
        "39": "/usr/local/tomcat/logs/dataLog.log",
        "dataLogMaxSize": "",
        "40": "/content/admin/remove?cache=pages&id=",
        "41": "/content/admin/remove?cache=templates&id=",
        "42": "/usr/local/tomcat/webapps/content/fileTransferFolder",
        "lookInContext": 1,
        "adminGroupID": 4,
        "betaServer": true}}],
  "servlet-mapping": {
    "43": "/",
    "44": "/cofaxutil/aemail/*",
    "45": "/admin/*",
    "46": "/static/*",
    "47": "/tools/*"},
 
  "taglib": {
    "48": "cofax.tld",
    "49": "/WEB-INF/tlds/cofax.tld"}}}
