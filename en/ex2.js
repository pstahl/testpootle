{"widget": {
    "1": "on",
    "window": {
        "2": "Sample Konfabulator Widget",
        "3": "main_window",
        "width": 500,
        "height": 500
    },
    "image": { 
        "4": "Images/Sun.png",
        "5": "sun1",
        "hOffset": 250,
        "vOffset": 250,
        "6": "center"
    },
    "text": {
        "7": "Click Here",
        "size": 36,
        "8": "bold",
        "9": "text1",
        "hOffset": 250,
        "vOffset": 100,
        "10": "center",
        "11": "sun1.opacity = (sun1.opacity / 100) * 90;"
    }
}}  
